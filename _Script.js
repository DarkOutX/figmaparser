module.exports = (Figma, data) => {
    data.forEach(dataset => {
        if (dataset.pos) {
            Figma.getNodeData(dataset.id, dataset.parent_id, dataset.params).then(res => {

                //postprocessing
                res.forEach(el => {
                    el.name = el.name.sh();
                })

                //output
                // fs.writeFile("lala.js", JSON.stringify(res, null, "\t"), () => { });
                console.log(res);
            });
        } else {
            Figma.getChildrenFromNode(dataset.id, dataset.path).then(svg_list => {
                console.log(svg_list);
                let elem = {};

                //postprocessing
                svg_list.forEach(svg => {
                    let name = svg.name.replace(/^Landmarks \/ (.+) \/ Normal$/, "$1").sh();
                    elem[svg.id] = name + "_Remall.svg";
                });

                //downloading
                Figma.downloadFile(elem, dataset.path, dataset.params)
                    .then(res => {
                        console.log("Downloaded")
                    });
            });
        }
    })
}