const FigmaParser = require("./FigmaParser");
const fs = require("fs");
const express = require("express");
const bodyParser = require('body-parser');
const data = require("./_Data.js");
const parse = require("./_Script.js");

String.prototype.shrink = function () {
    return this.replace(/[\/\ \-\_\']/g, "");
}
String.prototype.sh = String.prototype.shrink;

let Figma = new FigmaParser("Project", "Ff0f0f0ffFFff0FF0ffFff");

const ARGS = new Set(process.argv.slice(2));

let UPDATE = ARGS.has("update") || ARGS.has("u");
let PARSE_IT = ARGS.has("parser") || ARGS.has("p");
let USE_SERVER = ARGS.has("server") || ARGS.has("s"); //http://localhost:8080/

if (UPDATE) Figma.updateProject();
if (PARSE_IT) parse(Figma, data);

/**
 *   FFFFFF  RRRR    OOOOOOO  NN   NN  TTTTTTTT
 *   FF      RR RR   OO   OO  NNNN NN     TT
 *   FFFFF   RRRR    OO   OO  NN NNNN     TT
 *   FF      RR RR   OO   OO  NN  NNN     TT
 *   FF      RR  RR  OOOOOOO  NN   NN     TT
 */

if (USE_SERVER) {
    var app = express();

    // создаём маршрут для главной страницы
    // http://localhost:8080/
    app
        .use(express.static(__dirname + '/public'))
        .use(bodyParser.json())
        .get('/', function (req, res) {
            res.sendfile('./public/index.html');
        })
        .get('/update', (req, res, next) => {
            Figma.updateProject().then(() => {
                fs.writeFile("./public/project_min.js", "var project = " + JSON.stringify(Figma.getProject()), (e) => {
                    res.redirect("/");
                });
            }).catch(e => {
                next();
            })
        })
        .post('/addPreset', (req, res, next) => {
            let result = Figma.addPreset(req.body);
            res.send(result);
        })
        .get('/presetList', (req, res, next) => {
            res.send(JSON.stringify(Figma.getPresetList()));
        })
        .get('/preset/:name', (req, res, next) => {
            let preset = Figma.getPreset(req.params.name);
            if (preset) {
                let result = Figma.usePreset(req.params.name);
                res.send({
                    res: result
                })
            }
        })

    app.listen(8080);
    console.log('Сервер стартовал! \nАдрес:\nhttp://localhost:8080/\n\n');
}