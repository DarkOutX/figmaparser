module.exports = [
    {
        pos: true,
        id: "536:230159",
        parent_id: "456:360517",
        params: {
            isFolder: true,
            isCenter: false,
            getRotation: true,
            containsText: false
        }
    },
    {
        id: "536:230159",
        path: "landmarks",
        params: {
            format: "svg",
            svg_outline_text: true,
            svg_simplify_stroke: true,
        }
    }
]