# Figma-Parser
This project provides an ability to easily export data from your Figma projects

### Features

- Creating export presets for your projects
- Exporting SVG in bundles or one by one
- Optimizing project tree for easy traversal
- Multiple project support

### Installing

Just use `npm install` to install dependencies

### Getting started

Create file based on options/private.temp.js
> options/private.js

And write there your Figma API token.

Get a **FILE_KEY** of your Figma project 
> https://www.figma.com/file/**FILE_KEY**/

1. Load FigmaParser class into your index.js
2. Create an instance and specify project name and **FILE_KEY** 


```javascript
const FigmaParser = require("./FigmaParser");
let Figma = new FigmaParser("project", "Ff0f0f0ffFFff0FF0ffFff");
```

### File stucture:

+ **index.js** - entry file
+ **FigmaParser.js** - main class
+ **options/** - folder with options and presets
    * **projects/** - folder with project presets
	  + myproject.js - file with presets of your project named "myproject" 
    * **private.js** - file with your Figma API token
    * private.temp.js - example of private.js
    * **projects_map.js** - file with pairs **PROJECT_NAME**:*FILE_KEY*  
    * projects_map.temp.js - example of projects_map.js
+ **projects/** - folder with raw and minimized Figma project files 
    * Ff0f0f0ffFFff0FF0ffFff.js
    * Ff0f0f0ffFFff0FF0ffFff_min.js
+ **svg/** - folder to save SVGs
   * myproject/

### Methods:

+ **getProjectList()** - [static] returns list of available projects
+ **getProject()** - returns minimized project data
+ **updateProject()** - downloads project to work with it
+ **getPreset()** - returns existing presets of selected project
+ **usePreset()** - uses presets of selected project
+ **setPreset()** - overwrite all presets of selected project
+ **downloadSVG()** - downloads SVG in /svg/ folder with specified filename
 
 
 You can find a lot of additional info about all the methods in FigmaParser.js, it is commented with using JSDoc syntax
 
 ## Author

* **Zhukov Dmitry**  - [git: DarkOutX](https://gitlab.com/DarkOutX), [site: ZhukovD.ru](https://ZhukovD.ru)
 