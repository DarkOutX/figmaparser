const request = require("request");
const fs = require("fs");

if (!fs.existsSync("./options"))
    fs.mkdirSync("./options");
if (!fs.existsSync("./options/projects/"))
    fs.mkdirSync("./options/projects/");
if (!fs.existsSync("./options/private.js"))
    fs.writeFile("./options/private.js", "module.exports = " + JSON.stringify({ FIGMA_API_TOKEN: '00000-0f0fff00-f000-0ff0-00ff-f00ff0f000ff' }, null, "\t"), () => { })
if (!fs.existsSync("./options/projects_map.js"))
    fs.writeFile("./options/projects_map.js", "module.exports = {}", () => { })

const private_data = require("./options/private");
const projects = require("./options/projects_map");

const LOG_PREFIX = "[FigmaParser] ";
const API_KEY = private_data.FIGMA_API_TOKEN;

/**
 *  Methods:
 *      getProjectList() - [static] returns list of available projects
 *      getProject() - returns minimized project data
 *      updateProject() - redownloads project to work with it. Use this method if some of needed data has apperared or has changed it's ID
 *
 *      getChildrenFromNode() - gets all child elements from node
 *      downloadFile() - exports node in folder as a file
 *      getNodeData() - gets positions, rotation, width, height of specified node or it's children
 *      getNode() - gets all the data about specified node
 *
 *      getPreset() - returns existing presets of selected project
 *      usePreset() - uses presets of selected project
 *      addPreset() - add preset(s) to selected project
 *      setPreset() - overwrite all presets of selected project
 */
class FigmaParser {

    SELECTED_PROJECT = null; //String with project name
    FILE_KEY = null; //projects[this.SELECTED_PROJECT];
    PROJECT_PRESETS = null; //require("./options/projects/" + this.SELECTED_PROJECT);

    /**
     * @desc This class works with Figma project and gives an ability to download its SVGs
     * @param {String} project_name - Name of a project you want to use or download
     * @param {String} [project_key = null] - ID of a project from it's link (required only for the first time)
     * @example
     * new FigmaParser("My Project", "Ff0f0f0ffFFff0FF0ffFff") - will download project and save by name myproject
     * new FigmaParser("My Current Project") - will use project with name mycurrentproject
     */
    constructor(project_name, project_key = null) {
        //deleting special symbols, spaces, using lowercase
        if (project_name) {
            this.SELECTED_PROJECT = project_name.toLowerCase().replace(/[^a-z0-9]/gi, '');
        } else {
            console.log(LOG_PREFIX + "Project name is not specified");
            return;
        }

        if (projects[this.SELECTED_PROJECT]) {
            console.log(LOG_PREFIX + "Successfully found project: " + this.SELECTED_PROJECT);
            this.FILE_KEY = projects[this.SELECTED_PROJECT];
            this.PROJECT_PRESETS = require("./options/projects/" + this.SELECTED_PROJECT);
        } else if (project_key) {
            for (let project in projects)
                if (projects[project] == project_key) this.SELECTED_PROJECT = project;

            if (projects[this.SELECTED_PROJECT]) {
                this.FILE_KEY = projects[this.SELECTED_PROJECT];
                this.PROJECT_PRESETS = require("./options/projects/" + this.SELECTED_PROJECT);
            } else {
                //registering new project
                this.FILE_KEY = project_key;
                this.addProject();
            }
        } else {
            throw LOG_PREFIX + "Project not found and project key is not specified";
        }
    }

    //template for options. Each request setting it's own url and use it.
    request_options = {
        method: 'GET',
        url: "",
        headers: {
            'X-Figma-Token': API_KEY
        }
    };

    /**
     * @desc This method creates new project. Don't use it directly.
     */
    addProject() {
        let obj = this;
        return this.updateProject()
            .then(result => {
                if (result) {
                    projects[this.SELECTED_PROJECT] = this.FILE_KEY;
                    //updating projects map
                    let data = JSON.stringify(projects, null, "\t");

                    fs.writeFile("./options/projects_map.js", "module.exports = " + data, function (err, data) {
                        if (err) throw LOG_PREFIX + "Project not found and there is an error at creating projects map file";
                    });

                    //creating project options file
                    data = JSON.stringify({}, null, "\t");
                    fs.writeFile("./options/projects/" + obj.SELECTED_PROJECT + ".js", "module.exports = " + data, function (err, data) {
                        if (err)
                            throw LOG_PREFIX + "Project not found and there is an error at creating options file";
                        else
                            obj.PROJECT_PRESETS = require("./options/projects/" + obj.SELECTED_PROJECT);
                    });
                    console.log(LOG_PREFIX + "New project is successfully created");
                    return true;
                } else {
                    throw LOG_PREFIX + "Project not found and project key is not valid";
                }
            });
    }

    /**
     * @desc This method minimizes current project and saves it as js module which can be required. Don't use it directly.
     * @param {String} data - raw project data
     */
    async minimizeProject(data) {
        let TIMER = new Date().getTime();

        let data_min = [];

        /**
         * @desc Recursive function to build new object based on given data. 
         *  It iterates through global group, 
         *  if it see child elements, it going deeper and calling itself,
         *  if it doesn't see child elements, it could be counted as ending of recursy,
         *  but there could be multiple recursies, and you get a result only when all of them are ended 
         * @param {Object} item - Figma group
         * @returns {Object} Object { name:String, id:String, children:[] }
         */
        function createBlock(item) {
            let ar = item.children,
                block = {
                    name: item.name,
                    id: item.id
                };

            if (ar) {
                block.children = [];
                ar.forEach(elem => {
                    block.children.push(createBlock(elem));
                });
            }

            return block;
        };

        //project has global pages, which contains groups and layers you can see at the moment
        let pages = data.document.children;

        pages.forEach(page => {
            data_min.push(createBlock(page));
        });

        let dir = "projects/";
        let save_operation = await new Promise((resolve, reject) => {
            fs.writeFile(dir + this.FILE_KEY + "_min.js", "module.exports = " + JSON.stringify(data_min), function (err, data) {
                if (err) {
                    console.log(err);
                    resolve(false);
                }
                console.log(LOG_PREFIX + "Minimized project file in " + ((new Date().getTime() - TIMER) / 1000).toFixed(1) + "s");
                if (!fs.existsSync("./public")) fs.mkdirSync("./public");
                fs.writeFile("public/project_min.js", "var project = " + JSON.stringify(data_min), function (err, data) {
                    if (err) {
                        console.log(err);
                        resolve(false);
                    }
                    resolve(true);
                });
            });
        });

        return save_operation;
    }

    /**
     * @desc This method downloads project file and saves it as js module which can be required
     *  But project average weight is too big to work (25mb+), so I also minimize it by deleting some info
     *  Use this method only if project has changed it's structure or added some neccessary layers
     * @returns {Promise} Request and file save result
     */
    async updateProject() {
        let TIMER = new Date().getTime();

        this.request_options.url = "https://api.figma.com/v1/files/" + this.FILE_KEY;

        let obj = this;

        //requesting file
        let req_operation = await new Promise((resolve, reject) => {
            request(this.request_options, function (error, response, data) {
                let err = JSON.parse(data).err;
                if (err) console.log(err);

                let dir = "projects/";
                if (!fs.existsSync(dir)) {
                    fs.mkdirSync(dir);
                }
                //saving full project
                fs.writeFile(dir + obj.FILE_KEY + ".js", "module.exports = " + data, function (err, data) {
                    //requiring project (like additional success check)
                    const project = require("./projects/" + obj.FILE_KEY);

                    console.log(LOG_PREFIX + "Downloaded project file in " + ((new Date().getTime() - TIMER) / 1000).toFixed(1) + "s");

                    //rebuilding project without useless info 
                    obj.minimizeProject(project, obj.FILE_KEY)
                        .then(res => {
                            resolve(res);
                        });
                });
            });
        });

        return req_operation;
    }

    /**
     * @desc This method builds string of GET params from object
     * @param {Object} params - Object with params
     * @returns {String} GET-params
     */
    buildGetParams(params) {
        let output = [];
        for (let name in params) {
            output.push(name + "=" + params[name]);
        }
        return output.join("&");
    }

    /**
     * @desc This method saves file to project dir
     * @param {String} data - File content
     * @param {String} filename - Name of new file
     * @param {String} path - Path in project directory to save file
     * @param {Function} callback - Callback function
     */
    fileSave(data, filename, path, callback) {
        path = path.replace(/\/$/, "").replace(/^\//, "");
        let str_dir = "files/" + this.SELECTED_PROJECT + "/" + path,
            dir = "";

        //creating dirs if not exists
        str_dir.split("/").forEach(folder_name => {
            if (folder_name) dir += folder_name + "/";
            if (!fs.existsSync(dir)) {
                fs.mkdirSync(dir);
            }
        });

        fs.writeFile(dir + filename, data, (err) => {
            if (err) {
                console.log(err);
            } else {
                console.log(LOG_PREFIX + "Saved file " + dir + filename);
                if (callback) callback(true);
            }
        });
    }

    /**
     * @desc This method sending request for file data (Used to export SVG)
     * @param {String} url - Url to send request
     * @param {String} name - Name of new file
     * @param {String} path - Path in project directory to save file
     * @param {Boolean} download_res - Result of request
     */
    async downloadRequest(url, name, path) {
        let obj = this;

        let download_res = await new Promise((resolve, reject) => {
            this.request_options.url = url;

            request(this.request_options, function (err, response, data) {
                if (err) {
                    console.log(err);
                    resolve(false);
                } else {
                    obj.fileSave(data, name, path, resolve);
                }
            });
        })
            .catch(e => console.error(e));

        return download_res;
    }

    /**
     * @desc This method downloads Figma layers in SVG format with specified filename
     * @param {Object} elements - element data pairs in a form {*node_id*:*filename*}
     * @param {String} path - directory to save SVG in /svg/projectName/
     * @param {Object} params - parameters for downloading. { format: "svg", svg_outline_text: true, svg_simplify_stroke: true }
     * @returns {Promise} Request and file save result
     * @example downloadFile({"33:33":"aa.svg"}, "files/myf")
     */
    async downloadFile(elements, path, params) {
        if (!elements || Object.keys(elements) < 1) return false;

        if (!path) path = "";

        // here we set default params and rewriting them if user has defined some
        let defParams = {
            format: "svg",
            svg_outline_text: true,
            svg_simplify_stroke: true,
        };

        if (params)
            for (let paramName in defParams)
                if (params[paramName]) defParams[paramName] = params[paramName];

        // API doesn't allow us to send big amount of request for file links, so we have to send single request 
        // with all the IDs and then download all the files by separated async requests  
        let elem_ids = Object.keys(elements).join(",");

        this.request_options.url = "https://api.figma.com/v1/images/" + this.FILE_KEY + "?ids=" + elem_ids + "&" + this.buildGetParams(defParams);

        // getting links to download files
        let file_links = await new Promise((resolve, reject) => {
            request(this.request_options, function (error, response, data) {
                try {
                    let res = JSON.parse(data);
                    if (res.err) {
                        console.log(res.err);
                        resolve(false);
                    } else {
                        resolve(res.images);
                    }
                } catch (e) {
                    console.log(e);
                    resolve(false);
                }
            });
        });

        // downloading SVG
        let downloadPromises = [];
        if (file_links) {
            for (let node_id in file_links) {
                downloadPromises.push(this.downloadRequest(file_links[node_id], elements[node_id], path));
            }
            let result = await Promise.all(downloadPromises)
                .then(() => { return true })
                .catch(e => {
                    console.erroe(e);
                });
            return result;
        } else {
            return false;
        }
    }

    /**
     * @desc This method returns minimized project data
     * @returns {Object} Project tree
     */
    getProject() {
        let project_min = require("./projects/" + this.FILE_KEY + "_min");
        return project_min;
    }

    /**
     * @desc This method returns node
     * @returns {Object} Node
     */
    async getNode(id) {
        this.request_options.url = "https://api.figma.com/v1/files/" + this.FILE_KEY + "/nodes?ids=" + id;

        let ans = await new Promise((resolve, reject) => {
            request(this.request_options, function (error, response, data) {
                let res = JSON.parse(data);
                if (res.err) {
                    console.log(res.err);
                    resolve(false);
                } else {
                    resolve(res.nodes[id].document);
                }
            });
        });

        return ans;
    }

    /**
     * @static
     * @desc This method returns project list
     * @returns {Array} all existing projects
     */
    static getProjectList() {
        return Object.keys(projects);
    }

    /**
     * @desc This method finds and parses specified Figma group for inner layers.
     * @param {String} id_path - ID of a Figma element to parse all children layers
     * @param {String} path - directory to save SVGs in /files/#projectName#/
     * @returns {Array} Array of objects { id:String, name:String, path: String }
     */
    async getChildrenFromNode(id, path) {
        let result = await new Promise((resolve, reject) => {
            this.getNode(id)
                .then(node => {
                    let output = [];
                    node.children.forEach(subNode => {
                        output.push({
                            id: subNode.id,
                            name: subNode.name.replace(/^Landmarks \/ (.+) \/ Normal$/, "$1").replace(/[^A-Za-z0-9]/gi, ''),
                            path: path,
                        })
                    })
                    resolve(output);
                })
                .catch(e => {
                    console.log(LOG_PREFIX + "Can't get SVGs from node");
                    resolve(false)
                })
        })
        return result;
    }

    /**
     * @desc This method uses preconfigured preset to receive all mentioned data.
     * @param {String} [name=null] - name of preset, if not specified - using all 
     * @returns {undefined}
     */
    usePreset(name = null) {
        let obj = this,
            presets = obj.PROJECT_PRESETS;
        // project = obj.getProject();

        for (let preset in presets) {
            //use all presets or use only with specified name
            if ((name == null) || (preset == name)) {
                console.log(LOG_PREFIX + "Using preset " + name);
                //svg block
                let svg_preset = presets[preset].svg || [];
                svg_preset.forEach(svg => {
                    if (svg.is_folder) {
                        let svg_list = obj.getChildrenFromNode(svg.id, svg.path);
                        // let svg_list = obj.getSVGfromFolder(svg.id, project, svg.path);
                        if (svg_list) {
                            svg_list.forEach(svg => {
                                let elem = {};
                                elem[svg.id] = svg.name + ".svg";
                                obj.downloadFile(elem, svg.path)
                                    .then(res => {
                                        // console.log("Downloaded " + svg.name + ".svg")
                                    });
                            });
                        } else {
                            console.log(LOG_PREFIX + "Can't find layer with SVGs");
                        }
                    } else {
                        let elem = {};
                        elem[svg.id] = svg.name + ".svg";
                        obj.downloadFile(elem, svg.path)
                            .then(res => {
                                console.log(res);
                                // console.log("Downloaded " + svg.name + ".svg")
                            });
                    }
                });
                let pos_preset = presets[preset].pos || [];
                pos_preset.forEach(pos => {
                    obj.getNodeData(pos.id, pos.parent_id, { isFolder: pos.is_folder }).then(res => console.log(res))
                });
            }
        }
    }

    /**
     * @desc This method returns all preconfigured presets.
     * @param {String} [name=null] - name of preset, if not specified - returns all 
     * @returns {Object} preset
     */
    getPreset(name = null) {
        return (name) ? this.PROJECT_PRESETS[name] : this.PROJECT_PRESETS;
    }

    /**
     * @desc This method returns all preconfigured preset names.
     * @returns {Array} Preset names
     */
    getPresetList() {
        return Object.keys(this.PROJECT_PRESETS);
    }

    /**
     * @desc This method validates single preset.
     * @param {Object} preset - Single preset 
     * @returns {Object} Same preset with normal structure
     */
    validatePreset(preset) {
        let valid_preset = {};

        //svg block
        if (preset.svg) valid_preset.svg = [];
        preset.svg.forEach(elem => {
            let valid_elem = {};

            if (elem.is_folder) {
                valid_elem.is_folder = true;
            } else {
                if (elem.name) {
                    valid_elem.name = elem.name.replace(/[^A-Za-zА-Яа-яЁё._-]/gi, "");
                } else {
                    valid_elem.name = "def";
                    console.log(LOG_PREFIX + "Name in " + group + " was not defined and set \"def\"");
                }
            }

            if (elem.path) {
                valid_elem.path = elem.path.replace(/^\/?(.+)/, "$1").replace(/(.+)\/$/, "$1"); //removing slashes from the beginning and ending
            } else {
                console.log(LOG_PREFIX + "Path in " + group + " is not defined");
                valid_elem.path = "";
            }

            if (elem.id) {
                // valid_elem.id = (elem.is_folder)?elem.id:elem.id.split(",").pop();
                valid_elem.id = elem.id;
                valid_preset.svg.push(valid_elem);
            } else {
                console.log(LOG_PREFIX + "ID in " + group + " is not defined");
            }
        });

        //pos block
        if (preset.pos) valid_preset.pos = [];
        preset.pos.forEach(elem => {
            let valid_elem = {};

            if (elem.is_folder) {
                valid_elem.is_folder = true;
            } else {
                //it shouldn't be no folder at least in current version
                //TODO
                continue;
                console.log(LOG_PREFIX + "Pos preset must be is_folder in current version");
            }

            if (elem.path) {
                valid_elem.path = elem.path.replace(/^\/?(.+)/, "$1").replace(/(.+)\/$/, "$1");
            } else {
                console.log(LOG_PREFIX + "Path in " + group + " is not defined");
                valid_elem.path = "";
            }

            if (elem.parent_id && elem.id) {
                valid_elem.parent_id = elem.parent_id;
                valid_elem.id = elem.id;
                valid_preset.pos.push(valid_elem);
            } else {
                console.log(LOG_PREFIX + "PARENT_ID/ID in " + group + " is not defined");
            }
        });

        return valid_preset;
    }

    /**
     * @desc This method overwrites all(!) preconfigured presets for specified project.
     * @param {Object} presets - all presets need to be saved for current project 
     * @returns {undefined}
     */
    setPreset(presets) {
        // Validating received preset
        let valid_preset = {};
        for (let preset_name in preset) valid_preset[preset_name] = this.validatePreset(presets[preset_name]);

        let dir = "options/projects/",
            data = "module.exports = " + JSON.stringify(valid_preset, null, "\t");

        fs.writeFile(dir + this.SELECTED_PROJECT + ".js", data, function (err, data) {
            if (err) {
                console.log(err);
            } else {
                console.log(LOG_PREFIX + "Presets successfully updated");
            }
        });
    }

    /**
     * @desc This method adds preset(s) to project.
     * @param {Object} preset - object with new presets in a form {*PRESET_NAME*: *PRESET*} 
     * @returns {Boolean} Status of adding
     */
    addPreset(preset) {
        if (!preset) return;
        let curPreset = require(`./options/projects/${this.SELECTED_PROJECT}.js`);
        for (let name in preset) {
            if (name == "pos" || name == "svg") {
                consolw.log(LOG_PREFIX + "Seems like you're trying to add unnamed preset. Use addPreset({*PRESET_NAME*: *PRESET*})")
                continue;
            }
            if (curPreset[name]) {
                //пресет с таким именем уже есть
                console.log(LOG_PREFIX + `There is already preset with the name ${name}. New preset will be named ${name}_new`);
                curPreset[name + "_new"] = this.validatePreset(preset[name]);
            } else {
                curPreset[name] = this.validatePreset(preset[name]);
            }
        }

        let dir = "options/projects/",
            data = "module.exports = " + JSON.stringify(curPreset, null, "\t");

        fs.writeFile(dir + this.SELECTED_PROJECT + ".js", data, function (err, data) {
            if (err) {
                console.log(err);
                return false;
            } else {
                console.log(LOG_PREFIX + "Presets successfully updated");
                return true;
            }
        });
    }

    /***
     * @desc This method builds object with usefull data of node
     * 
     */
    parseElemData(elem, parent_pos, params) {
        let values = {
            name: elem.name,
            top: elem.absoluteBoundingBox.y - parent_pos.y,
            left: elem.absoluteBoundingBox.x - parent_pos.x,
            width: elem.absoluteBoundingBox.width,
            height: elem.absoluteBoundingBox.height,
        }

        if (params.isCenter) {
            values.top += values.height / 2;
            values.left += values.width / 2;
        }

        if (params.containsText && elem.children && elem.children[0].name) {
            values.name = elem.children[0].name;
        }

        let elementData = {
            name: values.name, //inner text = inner elem name
            top: Math.round(values.top),
            left: Math.round(values.left),
            width: Math.round(values.width),
            height: Math.round(values.height),
        }

        if (params.getRotation) {
            let matrix = elem.relativeTransform,
                rads = Math.atan2(-matrix[1][0], matrix[0][0]);
            elementData.rotation = rads * (180 / Math.PI);
        }

        return elementData;
    }

    /**
     * @desc This method gets positions, width, height, rotation of node or it's children elements.
     *          We usually use it to count positions of some labels, so I'm parsing inner text too and save it as node name.
     * @param {String} elem_id - ID of node which contains nodes to parse 
     * @param {String} parent_id - ID of node which position we count as 0:0 and use to count another positions. !!!REQUIRED!!! 
     * @param {Object} params - Optional. {isCenter: false, getRotation: false, containsText: false, isFolder: true}
     * @returns {Object} position of all elements
     */
    async getNodeData(elem_id, parent_id, params = null) {
        
        if (!params) params = {}
        // get coordinates from center
        if (!params.isCenter && params.isCenter !== false) params.isCenter = false;
        // get angle of transform: rotate()
        if (!params.getRotation && params.getRotation !== false) params.getRotation = false;
        // get name of inner first child's name
        if (!params.containsText && params.containsText !== false) params.containsText = false;
        // if we need to parse all the elements inside specified ID
        if (!params.isFolder && params.isFolder !== false) params.isFolder = true;

        let parent_pos,
            output_obj = [];

        // raw FIGMA positions are useless (it's like left: -30000px), so we have to define parent element to count padding from it
        if (!parent_id) return false;

        this.request_options.url = "https://api.figma.com/v1/files/" + this.FILE_KEY + "/nodes?ids=" + parent_id;

        //getting parent top and left
        parent_pos = await new Promise((resolve, reject) => {
            request(this.request_options, function (error, response, data) {
                let res = JSON.parse(data);
                if (res.err) {
                    console.warn(LOG_PREFIX + "Can't get parent element by specified ID")
                    console.log(res.err);
                    resolve(false);
                } else {
                    resolve(res.nodes[parent_id].document.absoluteBoundingBox);
                }
            });
        });

        if (!parent_pos) return;

        this.request_options.url = `https://api.figma.com/v1/files/${this.FILE_KEY}/nodes?ids=${elem_id}${(params.getRotation)?"&geometry=paths":""}`;

        //getting childs top and left
        let elem_data = await new Promise((resolve, reject) => {
            request(this.request_options, function (error, response, data) {
                let res = JSON.parse(data);
                if (res.err || !res.nodes[elem_id]) {
                    console.log(res.err);
                    resolve(false);
                } else {
                    resolve(res.nodes[elem_id].document);
                }
            });
        });

        if (!elem_data) return;

        // we'are counting centers of each element, so adding height/2 and width/2
        // I'm using feauture of Figma that it names layer as containing text, 
        // but this script requires container for that layer anyway
        if (!params.isFolder) {
            output_obj.push(this.parseElemData(elem_data, parent_pos, params));
        } else {
            if (elem_data.children) {
                elem_data.children.forEach(elem => {
                    output_obj.push(this.parseElemData(elem, parent_pos, params));
                })
            } else {
                console.warn(LOG_PREFIX + "Can't get elements by specified ID")
            }
        }

        return output_obj;
    }

    /**
     * @desc Incredibly Giant and probably still unfinished method to build preset from project with correct structure, specified below in "arch" var
     *          It should parse project and find essential nodes by names which we specified. Then it collects ID of that nodes or their children 
     *          and saves it to preset, which would get all the neccessary data just by call .usePreset('default') 
     * @returns {undefined}
     */
    //здесь мы должны спарсить дерево по именам, собрать все ID и собрать пресет 
    buildDefaultPreset() {
        let arch = [
            {
                name: "Main",
                children: [
                    {
                        name: "Map",
                        svg: [
                            {
                                name: "Landmarks",
                                is_folder: true,
                            }
                        ],
                        children: [
                            {
                                name: "Pins",
                                pos: [
                                    {
                                        name: "Retail",
                                        is_folder: true,
                                    },
                                    {
                                        name: "Education",
                                        is_folder: true,
                                    },
                                    {
                                        name: "Mosque",
                                        is_folder: true,
                                    },
                                    {
                                        name: "Health",
                                        is_folder: true,
                                    },
                                ]
                            }
                        ]
                    },
                    {
                        name: "Floorplans",
                        children: [
                            {
                                //unknown amount => no name
                                svg: [
                                    {
                                        name: "Floorplan_size",
                                    }
                                ],
                                pos: [
                                    {
                                        name: "Labels",
                                        is_folder: true,
                                    },
                                ]
                            }
                        ]
                    }
                ]
            }
        ];
        //end of arch tree

        //parsing project tree
        let project_tree = this.getProject(),
            preset = {};

        let addPresetSvg = (svg_data) => {
            if (!preset.svg) preset.svg = [];
            preset.svg.push(svg_data);
        };

        let addPresetPos = (pos_data) => {
            if (!preset.pos) preset.pos = [];
            preset.pos.push(pos_data);
        };

        let getTree = (nodes, arch_nodes, cur_path = "") => {

            let new_branch = [];

            nodes.forEach(node => {
                arch_nodes.forEach(desired_node => {
                    if (desired_node.name) {
                        //имя указано - ищем ноду
                        if (node.name.toLowerCase() == desired_node.name.toLowerCase()) {
                            //found desired node
                            let new_node = {
                                name: node.name,
                                id: node.id
                            };

                            if (desired_node.svg) { //блок проверки на svg
                                node.children.forEach(unnamed_node => {
                                    desired_node.svg.forEach(desired_svg => {
                                        if (unnamed_node.name == desired_svg.name) {
                                            if (desired_svg.is_folder) {
                                                let svg = {
                                                    name: unnamed_node.name,
                                                    is_folder: true,
                                                    path: desired_svg.name,
                                                    id: [cur_path, node.id, unnamed_node.id].join(",")
                                                };
                                                // console.log(unnamed_node.id);
                                                addPresetSvg(svg);
                                            } else {
                                                // addPresetSvg({name: node.name, id: unnamed_node.id, path: desired_svg.name});
                                            }
                                        }
                                    })
                                })
                            }

                            if (desired_node.pos) { //блок проверки на pos
                                node.children.forEach(unnamed_node => {
                                    desired_node.pos.forEach(desired_pos => {
                                        if (unnamed_node.name == desired_pos.name) {
                                            let pos = {
                                                name: desired_pos.name,
                                                path: node.name,
                                                parent_id: node.id,
                                                // id: unnamed_node.id,
                                                id: [cur_path, node.id, unnamed_node.id].join(",")
                                            };
                                            if (desired_pos.is_folder) pos.is_folder = true;

                                            addPresetPos(pos);
                                        }
                                    })
                                })
                            }

                            if (desired_node.children) { //if we need to go deeper
                                if (!new_node.children) new_node.children = [];
                                let next_path = (!cur_path) ? node.id : (cur_path + "," + node.id);
                                new_node.children.push(getTree(node.children, desired_node.children, next_path));
                            }
                            new_branch.push(new_node);
                        }
                    } else {
                        //имя не указано - берем все ноды
                        node.children.forEach(unnamed_node => {
                            let new_node = {
                                name: node.name,
                                id: node.id
                            };
                            if (desired_node.svg) { //блок проверки на svg
                                desired_node.svg.forEach(desired_svg => {
                                    if (unnamed_node.name == desired_svg.name) {
                                        if (desired_svg.is_folder) {

                                        } else {
                                            addPresetSvg({ name: node.name, id: unnamed_node.id, path: desired_svg.name });
                                        }
                                    }

                                })
                            }
                            if (desired_node.pos) { //блок проверки на pos
                                desired_node.pos.forEach(desired_pos => {
                                    if (unnamed_node.name == desired_pos.name) {
                                        let pos = {
                                            path: desired_pos.name,
                                            name: node.name,
                                            parent_id: node.id,
                                            // id: unnamed_node.id,
                                            id: [cur_path, node.id, unnamed_node.id].join(",")
                                        };
                                        if (desired_pos.is_folder) pos.is_folder = true;

                                        addPresetPos(pos);
                                    }
                                })
                            }
                            new_branch.push(new_node);
                        });
                    }
                })
            })

            return new_branch;
        }

        getTree(project_tree, arch);
        return { default: preset };
    };
}

module.exports = FigmaParser;